export const environment = {
  production: true,
  url: 'https://spirosscovid.herokuapp.com/',
  pusherId: 'c5c794ccb0fd68439318',
  pusherChannel: 'spirosscovid',
};
