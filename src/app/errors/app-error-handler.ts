import { ErrorHandler, Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { AppError } from './app-error.model';
import { AlertController, NavController } from '@ionic/angular';

@Injectable()
export class AppErrorHandler implements ErrorHandler {
  constructor(
    private alertController: AlertController,
    private nav: NavController
  ) {}

  handleError(error: Error | HttpErrorResponse) {
    // your custom error handling logic

    let err = null;
    if (error instanceof HttpErrorResponse) {
      let messageError = error.error.message || error['error']['msg'];
      messageError = JSON.stringify(messageError);
      err = new AppError(error.status, messageError, error);
    } else {
      if (error instanceof Error) {
        const messageError = error.message || error['msg'];
        err = new AppError(500, messageError, error);
      } else {
        err = error;
      }
    }
    console.log(err);
    this.presentAlert(err);
  }

  async presentAlert(err: AppError) {
    const alert = await this.alertController.create({
      cssClass: '',
      header: 'Error !!',
      message: `${err.message}.`,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            if (err.status === 401) {
              this.nav.navigateRoot('/main/tabs/user');
            }
          },
        },
      ],
    });

    await alert.present();
  }
}
