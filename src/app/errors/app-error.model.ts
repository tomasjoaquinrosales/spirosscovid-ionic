export class AppError {
  status: number;
  message: string;
  error: any;

  constructor(status: number = 500, message: string, error: any) {
    this.status = status;
    this.message = message;
    this.error = error;
  }
}
