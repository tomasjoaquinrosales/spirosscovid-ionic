import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import {
  AuthenticationResponse,
  Roles,
} from '../interfaces/authentication.interface';
import { environment } from 'src/environments/environment';
import { NavController } from '@ionic/angular';
import * as CryptoJS from 'crypto-js';
import { PusherProviderService } from './pusher-provider.service';

const URL = environment.url;
const GUARD = 'guardia';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  private username: string = null;
  private accessToken: string = null;
  private roles: Roles;

  constructor(
    private http: HttpClient,
    private storage: Storage,
    private nav: NavController
  ) {
    this.loadStorage();
  }

  login(username: string, password: string) {
    return new Promise((resolve) => {
      this.http
        .post(`${URL}/auth/login/`, {
          username,
          password,
        })
        .subscribe(
          (response: AuthenticationResponse) => {
            this.username = response.username;
            this.accessToken = response.access_token;
            this.saveInStorage(response, username);
            resolve(true);
          },
          (error) => {
            this.clearStorage();
            resolve(false);
            throw error;
          }
        );
    });
  }

  logout() {
    this.callLogOut();
  }

  clearStorage() {
    this.username = null;
    this.accessToken = null;
    this.storage.clear();
  }

  async loadStorage() {
    this.loadRoles();
    this.username = (await this.storage.get('username')) || null;
    this.accessToken = (await this.storage.get('access_token')) || null;
  }

  getAccessToken() {
    return this.accessToken;
  }

  getUsername() {
    return this.username;
  }

  async setRoles(): Promise<boolean> {
    return new Promise<boolean>((resolve) => {
      this.http.get<Roles>(`${URL}/auth/claim/`).subscribe(
        (response: Roles) => {
          this.roles = response;
          const encryptedRoles = CryptoJS.AES.encrypt(
            JSON.stringify(this.roles),
            'secret key 123'
          ).toString();
          this.storage.set('claims', encryptedRoles);
          resolve(true);
        },
        (error: any) => {
          resolve(false);
        }
      );
    });
  }

  getRoles() {
    return this.roles;
  }

  async loadRoles() {
    const encryptedRoles = (await this.storage.get('claims')) || null;
    if (encryptedRoles) {
      const bytes = CryptoJS.AES.decrypt(encryptedRoles, 'secret key 123');
      this.roles = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    }
  }

  saveInStorage(data: AuthenticationResponse, username: string) {
    this.storage.set('access_token', data.access_token);
    this.storage.set('username', username);
    // this.storage.set('refresh_token', data.refresh_token);
  }

  async isAuthenticated(): Promise<boolean> {
    await this.loadStorage();

    if (!this.accessToken) {
      return Promise.resolve(false);
    }

    return new Promise<boolean>((resolve) => {
      this.http.get(`${URL}/auth/token/valid/`).subscribe(
        (response: any) => {
          resolve(response.message);
        },
        (error: any) => {
          resolve(false);
        }
      );
    });
  }

  private callLogOut() {
    this.http.post(`${URL}/auth/logout/`, {}).subscribe((response) => {
      this.username = null;
      this.accessToken = null;
      this.roles = null;
      this.storage.clear();
      this.nav.navigateRoot('/login', { animated: true });
    });
  }

  // PERMISSIONS

  AMISuperuser() {
    if (this.roles.is_superuser) {
      return true;
    }
    return false;
  }

  AMIChief() {
    if (this.AMISuperuser() || this.roles.is_chief) {
      return true;
    }
    return false;
  }

  AMIStaff() {
    if (this.AMIChief() || this.roles.is_medic) {
      return true;
    }
    return false;
  }

  AMIGuard() {
    if (this.roles.system === GUARD) {
      return true;
    }
    return false;
  }

  getSystemName() {
    return this.roles['system'];
  }
}
