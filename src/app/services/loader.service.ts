import { Injectable, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root',
})
export class LoaderService {
  private load: any;

  constructor(private loadingController: LoadingController) {
    this.create();
  }

  async create() {
    this.load = await this.loadingController.create({
      message: 'Please wait...',
    });
  }

  async presentLoading() {
    await this.create();
    await this.load.present();
  }

  async dismissLoading() {
    this.load.dismiss();
  }
}
