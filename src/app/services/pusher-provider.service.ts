import { Injectable } from '@angular/core';
import Pusher, { Channel } from 'pusher-js';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { NotificationsService } from './notifications.service';

const PUSHER_ID = environment.pusherId;
const PUSHER_CHANNEL = environment.pusherChannel;

@Injectable({
  providedIn: 'root',
})
export class PusherProviderService {
  channel: Channel;
  rooms: number[] = [];
  private pusherNotificationsCount: number = 0;

  constructor(
    private http: HttpClient,
    private notification: NotificationsService
  ) {
    const pusher = new Pusher(PUSHER_ID, {
      cluster: 'us2',
    });
    this.channel = pusher.subscribe(PUSHER_CHANNEL);
    this.notification.loadNotifications().then((unread: number) => {
      this.pusherNotificationsCount = unread;
    });
  }

  init() {
    return this.channel;
  }

  add() {
    this.pusherNotificationsCount += 1;
  }

  subtract() {
    if (this.pusherNotificationsCount > 0) {
      this.pusherNotificationsCount -= 1;
    }
  }

  clean() {
    this.pusherNotificationsCount = 0;
  }

  getCount() {
    return this.pusherNotificationsCount;
  }
}
