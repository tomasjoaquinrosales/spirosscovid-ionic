import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Internement } from '../interfaces/internement.interface';
import { InternementSystem } from '../interfaces/system-internement.interface';
import { InternementListResponse } from '../interfaces/internement.interface';

const URL_INTERNEMENT = environment.url + '/internement';

@Injectable({
  providedIn: 'root',
})
export class InternementService {
  pageInternement = 0;

  constructor(private http: HttpClient) {}

  // LISTS

  getInternements(pull: boolean = true, patientId: number = null) {
    if (!pull) {
      this.pageInternement = 0;
    }
    let filter = '';
    if (patientId) {
      filter = `&patient=${patientId}`;
    }
    this.pageInternement++;
    return this.http.get<InternementListResponse>(
      `${URL_INTERNEMENT}/list/?page=${this.pageInternement}${filter}`
    );
  }

  getInternementByPatientId(patientId: number): Promise<Internement> {
    return new Promise<Internement>((resolve) => {
      this.http
        .get<Internement>(`${URL_INTERNEMENT}/patient/${patientId}/`)
        .subscribe((persistentInternement: Internement) => {
          resolve(persistentInternement);
        });
    });
  }

  // CREATES

  createInternement(internement: Internement): Promise<Internement> {
    return new Promise<Internement>((resolve) => {
      this.http
        .post(`${URL_INTERNEMENT}/`, internement)
        .subscribe((persistentInternement: Internement) => {
          resolve(persistentInternement);
        });
    });
  }

  addMedicsToInternement(patientId: number, medics: number[]) {
    const postObject = {
      medics,
    };
    return new Promise<Internement>((resolve) => {
      this.http
        .post(`${URL_INTERNEMENT}/${patientId}/medics/add/`, postObject)
        .subscribe((persistentInternement: Internement) => {
          resolve(persistentInternement);
        });
    });
  }

  changeInternementSystem(internementSystem: InternementSystem) {
    return new Promise<InternementSystem>((resolve) => {
      this.http
        .post(`${URL_INTERNEMENT}/change_system/`, internementSystem)
        .subscribe((persistentInternementSystem: InternementSystem) => {
          resolve(persistentInternementSystem);
        });
    });
  }

  closeInternement(
    internement: Internement,
    patientId: Number
  ): Promise<Internement> {
    return new Promise<Internement>((resolve) => {
      this.http
        .post(`${URL_INTERNEMENT}/patient/${patientId}/close/`, internement)
        .subscribe((persistentInternement: Internement) => {
          resolve(internement);
        });
    });
  }

  // VALIDATIONS

  hasInternement(patientId: number): Promise<boolean> {
    const patientObj = {
      id: patientId,
    };
    return new Promise<boolean>((resolve) => {
      this.http
        .post(`${URL_INTERNEMENT}/has/patient/${patientId}/`, patientObj)
        .subscribe((response) => {
          resolve(response['valid']);
        });
    });
  }
}
