import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import {
  SystemResponse,
  SystemBedResponse,
} from '../interfaces/system.interface';
import { InternementSystemResponse } from '../interfaces/system-internement.interface';

const URL_SYSTEM = environment.url + '/system';
const URL_INTERNEMENT = environment.url + '/internement';

@Injectable({
  providedIn: 'root',
})
export class SystemService {
  constructor(private http: HttpClient) {}

  getRooms(systemId?: number) {
    return this.http.get<SystemResponse>(
      `${URL_SYSTEM}/available_room/?system_id=${systemId}`
    );
  }

  getBeds(roomId: number) {
    return this.http.get<SystemBedResponse>(
      `${URL_SYSTEM}/available_bed/${roomId}/`
    );
  }

  getSystems() {
    return this.http.get<InternementSystemResponse>(`${URL_SYSTEM}/list/`);
  }

  getSystemsByPatientId(patientId: number) {
    return this.http.get<InternementSystemResponse>(
      `${URL_SYSTEM}/patient/${patientId}/list/`
    );
  }

  validateSystemBed(systemId: any) {
    return new Promise<boolean>((resolve) => {
      this.http
        .get<any>(`${URL_SYSTEM}/${systemId}/validate/beds/`)
        .subscribe((response) => {
          resolve(response['valid']);
        });
    });
  }
}
