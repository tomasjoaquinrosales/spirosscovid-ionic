import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

const URL_DASHBOARD = environment.url + '/system';

@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  constructor(private http: HttpClient) {}

  getDashboardSummary(systemId) {
    let system = '';
    if (systemId) {
      system = systemId;
    }
    return this.http.get(
      `${URL_DASHBOARD}/dashboard/summary/?systemId=${system}`
    );
  }
}
