import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import {
  NotificationListResponse,
  Notification,
} from '../interfaces/notification.interface';

const URL_NOTIFICATION = environment.url + '/notification';

@Injectable({
  providedIn: 'root',
})
export class NotificationsService {
  pageNotification = 0;

  constructor(private http: HttpClient) {}

  getNotifications(pull: boolean = true) {
    if (!pull) {
      this.pageNotification = 0;
    }
    this.pageNotification++;
    return this.http.get<NotificationListResponse>(
      `${URL_NOTIFICATION}/?page=${this.pageNotification}&pageSize=10`
    );
  }

  async loadNotifications() {
    return new Promise<number>((resolve) => {
      this.getNotifications(false).subscribe(
        (response: NotificationListResponse) => {
          resolve(response.unread);
        }
      );
    });
  }

  markNotifications(notificationId: number, isViewed: boolean = true) {
    let viewed = 'viewed';
    if (!isViewed) {
      viewed = 'unread';
    }
    return this.http.put<NotificationListResponse>(
      `${URL_NOTIFICATION}/${notificationId}/${viewed}/`,
      {}
    );
  }
}
