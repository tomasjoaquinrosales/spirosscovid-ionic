import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import {
  Medic,
  Patient,
  User,
  UserProfile,
  UserListResponse,
  UserProfileListResponse,
  MedicListResponse,
  PatientListResponse,
} from '../interfaces/users.interfaces';

const URL_USERS = environment.url + '/workforce';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  pageUser = 0;
  pageMedic = 0;
  pagePatient = 0;

  constructor(private http: HttpClient) {}

  // LISTS

  getUsers(pull: boolean = true) {
    if (!pull) {
      this.pageUser = 0;
    }
    this.pageUser++;
    return this.http.get<UserProfileListResponse>(
      `${URL_USERS}/user/list/?page=${this.pageUser}&pageSize=5`
    );
  }

  getMedics(pull: boolean = true) {
    if (!pull) {
      this.pageMedic = 0;
    }
    this.pageMedic++;
    return this.http.get<MedicListResponse>(
      `${URL_USERS}/medic/list/?page=${this.pageMedic}`
    );
  }

  getPatients(pull: boolean = true, filter: string = '') {
    if (!pull) {
      this.pagePatient = 0;
    }
    this.pagePatient++;
    return this.http.get<PatientListResponse>(
      `${URL_USERS}/patient/list/?page=${this.pagePatient}&patient=${filter}`
    );
  }

  // FILTERS

  getUserById(userId: number): Promise<User> {
    return new Promise<User>((resolve) => {
      this.http
        .get<User>(`${URL_USERS}/user/${userId}/`)
        .subscribe((persistentUser: User) => {
          resolve(persistentUser);
        });
    });
  }

  getPatientById(patientId: number): Promise<Patient> {
    return new Promise<Patient>((resolve) => {
      this.http
        .get<Patient>(`${URL_USERS}/patient/${patientId}/`)
        .subscribe((persistentPatient: Patient) => {
          resolve(persistentPatient);
        });
    });
  }

  getMedicByUsername(username: string): Promise<Medic> {
    return new Promise<Medic>((resolve) => {
      this.http
        .get<Patient>(`${URL_USERS}/medic/${username}/`)
        .subscribe((persistentMedic: Medic) => {
          resolve(persistentMedic);
        });
    });
  }

  // CREATES

  createMedic(userId: number, medic: Medic): Promise<Medic> {
    return new Promise<Medic>((resolve) => {
      this.http
        .post(`${URL_USERS}/medic/${userId}/`, medic)
        .subscribe((persistentMedic: Medic) => {
          resolve(persistentMedic);
        });
    });
  }

  createPatient(patient: Patient): Promise<Patient> {
    return new Promise<Patient>((resolve) => {
      this.http
        .post(`${URL_USERS}/patient/`, patient)
        .subscribe((persistentPatient: Patient) => {
          resolve(persistentPatient);
        });
    });
  }

  createUserProfile(
    user: User,
    userProfile: UserProfile
  ): Promise<UserProfile> {
    return new Promise<UserProfile>((resolve) => {
      this.http
        .post<User>(`${URL_USERS}/user/`, user)
        .subscribe((persistentUser: User) => {
          userProfile.user_id = persistentUser.id;
          console.log(userProfile);
          this.http
            .post<UserProfile>(
              `${URL_USERS}/user-profile/${persistentUser.id}/`,
              userProfile
            )
            .subscribe((persistentUserProfile: UserProfile) => {
              resolve(persistentUserProfile);
            });
        });
    });
  }
}
