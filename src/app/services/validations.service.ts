import { Injectable } from '@angular/core';
import { UsersService } from './users.service';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

const URL_USERS = environment.url + '/workforce';

@Injectable({
  providedIn: 'root',
})
export class ValidationsService {
  constructor(private userService: UsersService, private http: HttpClient) {}

  isFieldUniqueValid(
    field: string,
    value: any,
    workforce: string = 'user'
  ): Promise<boolean> {
    return new Promise<boolean>((resolve) => {
      this.http
        .get<any>(`${URL_USERS}/${workforce}/valid/${field}/${value}/`)
        .subscribe((response) => {
          resolve(response['valid']);
        });
    });
  }
}
