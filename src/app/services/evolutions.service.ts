import { Injectable } from '@angular/core';
import {
  Evolution,
  EvolutionListResponse,
} from '../interfaces/evolution.interface';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { EvolutionHistoryResponse } from '../interfaces/evolution-hisotry.interface';

const URL_EVOLUTION = environment.url + '/evolution';

@Injectable({
  providedIn: 'root',
})
export class EvolutionsService {
  pageEvolution = 0;

  constructor(private http: HttpClient) {}

  // LISTS

  getEvolutions(pull: boolean = true, patientId: number = null) {
    if (!pull) {
      this.pageEvolution = 0;
    }
    let filter = '';
    if (patientId) {
      filter = `&patient=${patientId}`;
    }
    this.pageEvolution++;
    return this.http.get<EvolutionListResponse>(
      `${URL_EVOLUTION}/list/?page=${this.pageEvolution}${filter}`
    );
  }

  getEvolutionHistory(patientId: number) {
    return this.http.get<EvolutionHistoryResponse>(
      `${URL_EVOLUTION}/list_evo_change/${patientId}`
    );
  }

  getEvolutionByPatientId(patientId: number): Promise<Evolution> {
    return new Promise<Evolution>((resolve) => {
      this.http
        .get<Evolution>(`${URL_EVOLUTION}/patient/${patientId}/`)
        .subscribe(
          (persistentEvolution: Evolution) => {
            resolve(persistentEvolution);
          },
          (error) => {
            resolve(null);
          }
        );
    });
  }

  createEvolution(evolution: Evolution) {
    return new Promise<Evolution>((resolve) => {
      this.http
        .post(`${URL_EVOLUTION}/`, evolution)
        .subscribe((persistentInternement: Evolution) => {
          resolve(evolution);
        });
    });
  }
}
