import { Medic } from './users.interfaces';

export interface EvolutionListResponse {
  results?: Evolution[];
  status?: number;
  totalResults?: number;
}

export interface Evolution {
  medic_id: number;
  internement_id: number;
  evolution_template: Evolutiontemplate;
  medical_study: Medicalstudy;
  treatment: Treatment;
  uti: Uti;
  created?: string;
  id?: number;
  is_active?: boolean;
  modified?: string;
  medic?: Medic;
  system_id?: number;
}

export interface Uti {
  arm?: boolean;
  arm_description?: any;
  tracheostomy?: boolean;
  vasopressors?: boolean;
  vasopressors_description?: any;
  created?: string;
  id?: number;
  is_active?: boolean;
  modified?: string;
}

export interface Treatment {
  name?: string;
  description?: any;
  created?: string;
  id?: number;
  is_active?: boolean;
  modified?: string;
}

export interface Medicalstudy {
  name?: string;
  description?: any;
  is_normal?: boolean;
  is_pathological?: boolean;
  created?: string;
  id?: number;
  is_active?: boolean;
  modified?: string;
}

export interface Evolutiontemplate {
  anosmia?: boolean;
  blood_pressure_d?: number;
  blood_pressures_s?: number;
  breathing_frequency?: number;
  cardiac_frequency?: number;
  cough?: boolean;
  created?: string;
  drowsiness?: boolean;
  dysguesia?: boolean;
  dyspnoea?: boolean;
  id?: number;
  is_active?: boolean;
  is_o2_required?: boolean;
  is_pafi_required?: boolean;
  modified?: string;
  o2?: string;
  o2_saturation?: number;
  observation?: string;
  pafi?: number;
  prono_vigil?: boolean;
  respiratory_symtoms?: boolean;
  temperature?: number;
  ventilator?: string;
  evolution_date?: Date;
}
