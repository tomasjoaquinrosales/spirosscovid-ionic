export interface AuthenticationResponse {
  username: string;
  access_token: string;
  //refresh_token: string;
}

export interface Roles {
  is_chief?: boolean;
  is_manager?: boolean;
  is_medic?: boolean;
  is_superuser?: boolean;
  system?: string;
}
