import { Patient } from './users.interfaces';
export interface DashboardResponse {
  result?: Result;
}

export interface Result {
  rooms?: Room[];
}

export interface Room {
  name?: string;
  beds?: number;
  id?: number;
  occupation?: number;
  patients?: Patient[];
}
