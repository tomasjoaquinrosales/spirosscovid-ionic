import { Room } from './dashboard.interface';
export interface MedicListResponse {
  results: Medic[];
  status: number;
  totalResults: number;
}

export interface UserListResponse {
  results?: User[];
  status?: number;
  totalResults?: number;
}

export interface UserProfileListResponse {
  results?: UserProfile[];
  status?: number;
  totalResults?: number;
}

export interface PatientListResponse {
  results?: Patient[];
  status?: number;
  totalResults?: number;
}

export interface Medic {
  created?: string;
  docket?: number;
  id?: number;
  is_active?: boolean;
  is_chief?: boolean;
  modified?: string;
  user?: User;
  user_id?: number;
}

export interface Patient {
  created?: string;
  id?: number;
  is_active?: boolean;
  address?: string;
  medical_coverage?: string;
  modified?: string;
  other_information?: string;
  date_of_birth?: string;
  dni?: string;
  first_name?: string;
  last_name?: string;
  telephone?: string;
  is_int_active?: any;
  room?: Room;
  bed?: any;
  is_dead?: boolean;
}

export interface UserProfile {
  address?: string;
  created?: string;
  date_of_birth?: string;
  dni?: string;
  first_name?: string;
  id?: number;
  is_active?: boolean;
  last_name?: string;
  modified?: string;
  telephone?: string;
  user?: User;
  user_id?: number;
}

export interface User {
  id?: number;
  is_active?: boolean;
  is_manager?: boolean;
  is_medic?: boolean;
  is_superuser?: boolean;
  username?: string;
  password?: string;
}
