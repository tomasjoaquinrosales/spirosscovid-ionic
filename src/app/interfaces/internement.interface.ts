import { Medic, Patient } from './users.interfaces';

export interface InternementListResponse {
  results?: Internement[];
  status?: number;
  totalResults?: number;
}

export interface Internement {
  check_in?: string;
  check_out?: string;
  created?: string;
  current_disease?: string;
  date_diagnosis?: string;
  date_first_simptoms?: string;
  date_of_death?: string;
  id?: number;
  is_active?: boolean;
  medic?: Medic[];
  modified?: string;
  patient?: Patient;
  patient_id?: number;
  medics_ids?: number[];
  room_id?: number;
  bed_id?: number;
  bed_label?: string;
  death_observation?: string;
}
