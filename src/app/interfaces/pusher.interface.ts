import { Medic, Patient } from './users.interfaces';

export interface PusherResponse {
  notification: Notification;
  active_room: ActiveRoom;
}

export interface Notification {
  id: number;
  medic_id: number;
  modified: string;
  is_active: boolean;
  patient: Patient;
  medic: Medic;
  created: string;
  patient_id: number;
}

export interface ActiveRoom {
  active?: string;
  room?: string;
  medics?: string[];
}
