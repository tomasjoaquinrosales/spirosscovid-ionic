export interface SystemResponse {
  response?: SystemRoom[];
}

export interface SystemRoom {
  created?: string;
  id?: number;
  is_active?: boolean;
  is_room_infinite?: boolean;
  modified?: string;
  name?: string;
}

export interface SystemBedResponse {
  response?: SystemBed[];
}

export interface SystemBed {
  created?: string;
  id?: number;
  is_active?: boolean;
  label?: string;
  modified?: string;
}
