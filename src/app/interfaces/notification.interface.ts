import { Medic, Patient } from './users.interfaces';
export interface NotificationListResponse {
  results?: Notification[];
  status?: number;
  totalResults?: number;
  unread?: number;
}

export interface Notification {
  created?: string;
  id?: number;
  is_active?: boolean;
  medic?: Medic;
  medic_id?: number;
  modified?: string;
  patient?: Patient;
  patient_id?: number;
  viewed?: boolean;
  message?: string;
}
