import {
  Uti,
  Treatment,
  Medicalstudy,
  Evolutiontemplate,
} from './evolution.interface';
import { Internement } from './internement.interface';
import { Medic } from './users.interfaces';
export interface EvolutionHistoryResponse {
  results?: EvolutionHistory[];
}

export interface EvolutionHistory {
  date?: string;
  evolutions?: Evolution[];
  id?: number;
  internement_id?: number;
  medic_id?: number;
  system?: string;
  system_id?: number;
}

export interface Evolution {
  created?: string;
  evolution_template?: Evolutiontemplate;
  id?: number;
  internement?: Internement;
  is_active?: boolean;
  medic?: Medic;
  medical_study?: Medicalstudy;
  modified?: string;
  system_id?: number;
  treatment?: Treatment;
  uti?: Uti;
}
