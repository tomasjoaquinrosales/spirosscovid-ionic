import { Medic } from './users.interfaces';

export interface InternementSystemResponse {
  results: System[];
  status: number;
  totalResults: number;
}

export interface System {
  chief_id?: number;
  created: string;
  has_infinite_beds: boolean;
  id: number;
  is_active: boolean;
  medics: Medic[];
  modified: string;
  name: string;
  rooms: Room[];
}

export interface Room {
  id: number;
  is_active: boolean;
  is_room_infinite: boolean;
  name: string;
}

export interface InternementSystem {
  date?: any;
  internement_id: number;
  system_id: number;
  medic_id: number;
  bed_id: number;
  bed_label?: any;
  room_id?: number;
}
