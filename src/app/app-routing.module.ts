import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'login',
  },
  {
    path: 'main',
    loadChildren: () =>
      import('./pages/tabs/tabs.module').then((m) => m.TabsPageModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./pages/login/login.module').then((m) => m.LoginPageModule),
  },
  {
    path: 'user-attach',
    loadChildren: () =>
      import('./pages/user-attach/user-attach.module').then(
        (m) => m.UserAttachPageModule
      ),
  },
  {
    path: 'search-modal',
    loadChildren: () =>
      import('./pages/search-modal/search-modal.module').then(
        (m) => m.SearchModalPageModule
      ),
  },  {
    path: 'internement-detail',
    loadChildren: () => import('./pages/internement-detail/internement-detail.module').then( m => m.InternementDetailPageModule)
  },
  {
    path: 'patient-detail',
    loadChildren: () => import('./pages/patient-detail/patient-detail.module').then( m => m.PatientDetailPageModule)
  },
  {
    path: 'patient-detail',
    loadChildren: () => import('./pages/patient-detail/patient-detail.module').then( m => m.PatientDetailPageModule)
  },
  {
    path: 'patient-internement-detail',
    loadChildren: () => import('./pages/patient-internement-detail/patient-internement-detail.module').then( m => m.PatientInternementDetailPageModule)
  },
  {
    path: 'evolution-detail',
    loadChildren: () => import('./pages/evolution-detail/evolution-detail.module').then( m => m.EvolutionDetailPageModule)
  },
  {
    path: 'evolution-modal',
    loadChildren: () => import('./pages/evolution-modal/evolution-modal.module').then( m => m.EvolutionModalPageModule)
  },
  {
    path: 'patient-evolution-detail',
    loadChildren: () => import('./pages/patient-evolution-detail/patient-evolution-detail.module').then( m => m.PatientEvolutionDetailPageModule)
  },
  {
    path: 'patient-system',
    loadChildren: () => import('./pages/patient-system/patient-system.module').then( m => m.PatientSystemPageModule)
  },
  {
    path: 'patient-internemenet-closed',
    loadChildren: () => import('./pages/patient-internemenet-closed/patient-internemenet-closed.module').then( m => m.PatientInternemenetClosedPageModule)
  },

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
