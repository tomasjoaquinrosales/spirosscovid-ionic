import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  //components: Observable<ItemComponent[]>;
  components: any;

  constructor() {}

  ngOnInit() {
    //this.components = this.dataService.getMenuItem();
    this.components = [
      {
        icon: 'american-football',
        name: 'Action Sheet',
        redirecTo: '/action-sheet',
      },
      {
        icon: 'logo-google-playstore',
        name: 'Alert',
        redirecTo: '/alert',
      },
    ];
  }
}
