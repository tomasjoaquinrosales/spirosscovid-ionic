import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

// COMPONENTS

import { MenuComponent } from './menu/menu.component';
import { CardComponent } from './card/card.component';
import { CardsComponent } from './cards/cards.component';
import { HeaderComponent } from './header/header.component';
import { InformationModalComponent } from './information-modal/information-modal.component';
import { PipesModule } from '../pipes/pipes.module';
import { DashboardRoomComponent } from './dashboard-room/dashboard-room.component';
import { ChartsModule } from 'ng2-charts';
import { NoInformationAvailableComponent } from './no-information-available/no-information-available.component';

@NgModule({
  declarations: [
    MenuComponent,
    CardsComponent,
    CardComponent,
    HeaderComponent,
    InformationModalComponent,
    DashboardRoomComponent,
    NoInformationAvailableComponent,
  ],
  imports: [CommonModule, IonicModule, PipesModule, ChartsModule],
  exports: [
    MenuComponent,
    CardsComponent,
    CardComponent,
    HeaderComponent,
    InformationModalComponent,
    DashboardRoomComponent,
    NoInformationAvailableComponent,
  ],
})
export class ComponentsModule {}
