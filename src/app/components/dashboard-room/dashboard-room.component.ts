import { Component, Input, OnInit } from '@angular/core';
import { ChartType, ChartOptions } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-dashboard-room',
  templateUrl: './dashboard-room.component.html',
  styleUrls: ['./dashboard-room.component.scss'],
})
export class DashboardRoomComponent implements OnInit {
  @Input() occupation: number = 0;
  @Input() freeBeds: number = 0;
  drawChart: boolean = false;
  pieChartOptions: ChartOptions = {
    responsive: false,
    legend: {
      position: 'bottom',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    },
    title: {
      display: true,
      text: 'Ocupación De Camas',
    },
    animation: {
      duration: 1000,
      easing: 'easeInCubic',
      animateRotate: true,
      animateScale: true,
    },
  };
  pieChartLabels: Label[];
  pieChartData: number[];
  pieChartType: ChartType = 'pie';
  pieChartLegend = false;
  pieChartPlugins = [pluginDataLabels];
  pieChartColors = [
    {
      backgroundColor: [
        'rgba(255,0,0,0.3)',
        'rgba(0,255,0,0.3)',
        'rgba(0,0,255,0.3)',
      ],
    },
  ];

  constructor() {}

  ngOnInit() {
    this.pieChartLabels = ['ocupadas', 'libres'];
    this.pieChartData = [this.occupation, this.freeBeds];
    this.pieChartType = 'pie';
    this.pieChartLegend = false;
    this.pieChartPlugins = [pluginDataLabels];
    this.drawChart = true;
  }

  ionViewWillEnter() {}

  // events
  public chartClicked({
    event,
    active,
  }: {
    event: MouseEvent;
    active: {}[];
  }): void {
    console.log(event, active);
  }

  public chartHovered({
    event,
    active,
  }: {
    event: MouseEvent;
    active: {}[];
  }): void {
    console.log(event, active);
  }
}
