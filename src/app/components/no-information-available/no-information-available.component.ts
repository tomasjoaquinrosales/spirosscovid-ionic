import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-information-available',
  templateUrl: './no-information-available.component.html',
  styleUrls: ['./no-information-available.component.scss'],
})
export class NoInformationAvailableComponent implements OnInit {

  constructor() { }

  ngOnInit() {}

}
