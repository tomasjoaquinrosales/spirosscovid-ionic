import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-information-modal',
  templateUrl: './information-modal.component.html',
  styleUrls: ['./information-modal.component.scss'],
})
export class InformationModalComponent implements OnInit {
  @Input() info;
  @Input() title;
  dataKeys: string[] = [];
  dataValues: any[] = [];

  constructor() {}

  ngOnInit() {
    this.dataKeys = Object.keys(this.info).map((key) => key);
    this.dataValues = Object.values(this.info).map((value) => value);
  }
}
