import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input() title: string;
  @Input() subtitle: string;
  @Input() internementActive: boolean;
  @Input() buttonsActionsActive: boolean;
  @Input() contents: any[];
  @Input() callBack1: CallbackInterface = {
    isActive: false,
    icon: 'ban-outline',
  };
  @Input() callBack2: CallbackInterface = {
    isActive: false,
    icon: 'ban-outline',
  };
  @Input() callBack3: CallbackInterface = {
    isActive: false,
    icon: 'ban-outline',
  };
  @Input() callBack4: CallbackInterface = {
    isActive: false,
    icon: 'heart-half-outline',
  };
  @Output() callbackEvent1: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() callbackEvent2: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() callbackEvent3: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() callbackEvent4: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private authService: AuthenticationService) {}

  ngOnInit() {}

  AMIGuard() {
    return this.authService.AMIGuard();
  }

  callback1() {
    this.callbackEvent1.emit(true);
  }

  callback2() {
    if (this.AMIGuard()) {
      this.callbackEvent2.emit(true);
    }
  }

  callback3() {
    this.callbackEvent3.emit(true);
  }

  callback4() {
    this.callbackEvent4.emit(true);
  }
}

interface CallbackInterface {
  isActive: boolean;
  icon: string;
}
