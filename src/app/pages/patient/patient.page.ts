import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from '../../services/users.service';
import { ValidationsService } from '../../services/validations.service';
import { Patient } from '../../interfaces/users.interfaces';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.page.html',
  styleUrls: ['./patient.page.scss'],
})
export class PatientPage implements OnInit {
  patient: Patient = {
    medical_coverage: null,
    other_information: null,
    date_of_birth: null,
    dni: null,
    first_name: null,
    last_name: null,
    telephone: null,
  };
  patientForm = new FormGroup({
    dni: new FormControl(this.patient.dni, [
      Validators.required,
      Validators.minLength(8),
      Validators.maxLength(8),
    ]),
    firstName: new FormControl(this.patient.first_name, [
      Validators.required,
      Validators.minLength(4),
    ]),
    lastName: new FormControl(this.patient.last_name, [
      Validators.required,
      Validators.minLength(4),
    ]),
    address: new FormControl(this.patient.address, [
      Validators.required,
      Validators.minLength(4),
    ]),
    telephone: new FormControl(this.patient.telephone, [Validators.required]),
    dateOfBirth: new FormControl(this.patient.date_of_birth, [
      Validators.required,
    ]),
    medicalCoverage: new FormControl(this.patient.medical_coverage, [
      Validators.required,
      Validators.minLength(4),
    ]),
    otherInformation: new FormControl(this.patient.other_information, [
      Validators.required,
      Validators.minLength(4),
    ]),
  });

  constructor(
    private router: Router,
    private userService: UsersService,
    private validationsService: ValidationsService
  ) {}

  ngOnInit() {}

  async validateDNI() {
    let responseValue = await this.validationsService.isFieldUniqueValid(
      'dni',
      this.patient.dni,
      'patient'
    );
    if (!responseValue) {
      this.patientForm.controls['dni'].setErrors({ incorrect: true });
    }
  }

  async createUserPatient() {
    if (this.patientForm.valid) {
      const format = 'yyyy-MM-dd';
      const locale = 'en-US';
      const formattedDate = formatDate(
        this.patient.date_of_birth,
        format,
        locale
      );
      this.patient.date_of_birth = formattedDate;
      const patient = await this.userService.createPatient(this.patient);
      this.router.navigateByUrl('/main/tabs/user');
    }
  }

  reset() {
    if (this.patientForm.valid) {
      this.patientForm.reset();
    }
    this.patient = {
      medical_coverage: null,
      other_information: null,
    };
  }
}
