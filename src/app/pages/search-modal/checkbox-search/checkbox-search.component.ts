import { Component, Input, OnInit } from '@angular/core';
import { User, Medic } from '../../../interfaces/users.interfaces';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-checkbox-search',
  templateUrl: './checkbox-search.component.html',
  styleUrls: ['./checkbox-search.component.scss'],
})
export class CheckboxSearchComponent implements OnInit {
  @Input() medics: Medic[] = [];
  users: User[] = [];

  constructor(private modalController: ModalController) {}

  ngOnInit() {}

  filterList(ev) {}

  selectUser(item) {}

  addToList(item, event) {
    if (event.detail.checked && !this.users.includes(item)) {
      this.users.unshift(item);
    } else {
      if (!event.detail.checked) {
        const pos = this.users.findIndex((elem) => elem === item);
        this.users.splice(pos, 1);
      }
    }
  }

  dismissModal() {
    this.modalController.dismiss(this.users);
  }
}
