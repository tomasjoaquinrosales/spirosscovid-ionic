import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { UsersService } from '../../services/users.service';
import { Patient, Medic } from '../../interfaces/users.interfaces';

@Component({
  selector: 'app-search-modal',
  templateUrl: './search-modal.page.html',
  styleUrls: ['./search-modal.page.scss'],
})
export class SearchModalPage implements OnInit {
  // Data passed in by componentProps
  @Input() title: string;
  @Input() kindOfSearch: string;
  @Input() isMultiCheck: boolean = false;

  users: any[] = [];

  constructor(
    private modalController: ModalController,
    private userService: UsersService
  ) {}

  async ngOnInit() {
    if (this.kindOfSearch === 'patient') {
      this.users = await (await this.userService.getPatients(false).toPromise())
        .results;
    } else {
      if (this.kindOfSearch === 'medic') {
        this.users = await (await this.userService.getMedics(false).toPromise())
          .results;
      }
    }
  }

  private initializeItems() {
    if (this.kindOfSearch === 'patient') {
      return this.userService.getPatients(false).toPromise();
    } else {
      if (this.kindOfSearch === 'medic') {
        return this.userService.getMedics(false).toPromise();
      }
    }
  }

  async filterList(event) {
    this.users = await (await this.initializeItems()).results;
    const searchTerm = event.srcElement.value;

    if (!searchTerm) {
      return;
    }

    this.users = this.users.filter((currentUser) => {
      if (currentUser.user) {
        if (currentUser.user.username && searchTerm) {
          return (
            currentUser.user.username
              .toLowerCase()
              .indexOf(searchTerm.toLowerCase()) > -1
          );
        }
      } else {
        if (currentUser.first_name && searchTerm) {
          return (
            currentUser.first_name
              .toLowerCase()
              .indexOf(searchTerm.toLowerCase()) > -1
          );
        }
      }
    });
  }

  selectUser(user: any): void {
    this.modalController.dismiss(user);
  }
}
