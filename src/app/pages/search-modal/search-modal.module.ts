import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SearchModalPageRoutingModule } from './search-modal-routing.module';

import { SearchModalPage } from './search-modal.page';

// COMPONENT
import { CheckboxSearchComponent } from './checkbox-search/checkbox-search.component';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SearchModalPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [SearchModalPage, CheckboxSearchComponent],
})
export class SearchModalPageModule {}
