import { Component, OnInit, ViewChild } from '@angular/core';
import { PusherProviderService } from '../../services/pusher-provider.service';
import { NotificationsService } from '../../services/notifications.service';
import { IonInfiniteScroll } from '@ionic/angular';
import {
  NotificationListResponse,
  Notification,
} from 'src/app/interfaces/notification.interface';
import { LoaderService } from 'src/app/services/loader.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss'],
})
export class Tab3Page {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  notifications: Notification[] = [];
  loading = true;
  constructor(
    private pusher: PusherProviderService,
    private notification: NotificationsService,
    private loader: LoaderService
  ) {}

  ionViewDidEnter(): void {
    this.doRefresh(false);
  }

  doRefresh(event) {
    this.loader.presentLoading();
    this.loading = true;
    this.notification
      .getNotifications(false)
      .subscribe((response: NotificationListResponse) => {
        this.notifications = response.results;
        this.pusher.clean();
        for (let index = 0; index < response.unread; index++) {
          this.pusher.add();
        }
        if (event) {
          event.target.complete();
        }
        this.loader.dismissLoading();
        this.loading = false;
      });
  }

  loadData(event) {
    this.notification.getNotifications(true).subscribe((response) => {
      if (this.notifications.length === response.totalResults) {
        event.target.disabled = true;
      } else {
        this.notifications.push(...response.results);
        event.target.complete();
      }
    });
  }

  read(notification: Notification) {
    this.notification
      .markNotifications(notification.id)
      .subscribe((response) => {
        notification.viewed = true;
        this.pusher.subtract();
      });
  }

  unread(notification: Notification) {
    this.notification
      .markNotifications(notification.id, false)
      .subscribe((response) => {
        notification.viewed = false;
        this.pusher.add();
      });
  }
}
