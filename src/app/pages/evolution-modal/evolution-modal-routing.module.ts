import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EvolutionModalPage } from './evolution-modal.page';

const routes: Routes = [
  {
    path: '',
    component: EvolutionModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EvolutionModalPageRoutingModule {}
