import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import {
  Medicalstudy,
  Treatment,
  Uti,
} from 'src/app/interfaces/evolution.interface';

@Component({
  selector: 'app-evolution-modal',
  templateUrl: './evolution-modal.page.html',
  styleUrls: ['./evolution-modal.page.scss'],
})
export class EvolutionModalPage implements OnInit {
  @Input() title: string;
  @Input() kindOfSearch: string;
  medicalStudy: Medicalstudy = {
    name: null,
    description: null,
    is_normal: true,
    is_pathological: false,
  };
  treatment: Treatment = {
    name: null,
    description: null,
  };
  uti: Uti = {
    arm: false,
    arm_description: null,
    tracheostomy: false,
    vasopressors: false,
    vasopressors_description: null,
  };
  medicalStudyForm = new FormGroup({
    name: new FormControl(this.medicalStudy.name, [Validators.required]),
    isNormal: new FormControl(this.medicalStudy.is_normal, []),
    isPathological: new FormControl(this.medicalStudy.is_pathological, []),
    description: new FormControl(this.medicalStudy.description, []),
  });
  treatmentForm = new FormGroup({
    name: new FormControl(this.treatment.name, [Validators.required]),
    description: new FormControl(this.treatment.description, []),
  });
  utiForm = new FormGroup({
    arm: new FormControl(this.uti.arm, [Validators.required]),
    tracheostomy: new FormControl(this.uti.tracheostomy, []),
    vasopressors: new FormControl(this.uti.vasopressors, []),
    armDescription: new FormControl(this.uti.arm_description, []),
    vasopressorsDescription: new FormControl(
      this.uti.vasopressors_description,
      []
    ),
  });

  constructor(private modalController: ModalController) {}

  async ngOnInit() {
    if (this.kindOfSearch === 'ms') {
      console.log('medical study');
    } else {
      if (this.kindOfSearch === 't') {
        console.log('treatment');
      } else {
        if (this.kindOfSearch === 'uti') {
          console.log('uti');
        }
      }
    }
  }

  createMedicalStudy() {
    if (this.medicalStudyForm.valid) {
      this.modalController.dismiss(this.medicalStudy);
    }
  }

  createTreatment() {
    if (this.treatmentForm.valid) {
      this.modalController.dismiss(this.treatment);
    }
  }

  createUTI() {
    if (this.utiForm.valid) {
      this.modalController.dismiss(this.uti);
    }
  }
}
