import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EvolutionModalPageRoutingModule } from './evolution-modal-routing.module';

import { EvolutionModalPage } from './evolution-modal.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    EvolutionModalPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [EvolutionModalPage],
})
export class EvolutionModalPageModule {}
