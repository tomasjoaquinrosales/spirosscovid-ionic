import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EvolutionModalPage } from './evolution-modal.page';

describe('EvolutionModalPage', () => {
  let component: EvolutionModalPage;
  let fixture: ComponentFixture<EvolutionModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvolutionModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EvolutionModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
