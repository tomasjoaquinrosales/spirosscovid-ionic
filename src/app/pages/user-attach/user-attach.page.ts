import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators,
  AbstractControl,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Patient, Medic, User } from '../../interfaces/users.interfaces';
import { UsersService } from '../../services/users.service';
import { ValidationsService } from '../../services/validations.service';

@Component({
  selector: 'app-user-attach',
  templateUrl: './user-attach.page.html',
  styleUrls: ['./user-attach.page.scss'],
})
export class UserAttachPage implements OnInit {
  medic: Medic = {
    docket: null,
    is_chief: false,
  };
  medicForm = new FormGroup({
    docket: new FormControl(this.medic.docket, [
      Validators.required,
      Validators.minLength(3),
      Validators.pattern('^[0-9]*$'),
    ]),
    isChief: new FormControl(this.medic.docket, [Validators.required]),
  });
  user: User = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UsersService,
    private validationsService: ValidationsService
  ) {}

  async ngOnInit() {
    const userId = Number(this.route.snapshot.paramMap.get('id'));
    this.user = await this.userService.getUserById(userId);
  }

  async validateDocket() {
    const isValid = await this.validationsService.isFieldUniqueValid(
      'docket',
      this.medic.docket
    );
    if (!isValid) {
      this.medicForm.controls['docket'].setErrors({ incorrect: true });
    }
  }

  async createUserMedic() {
    if (this.medicForm.valid) {
      this.medic.user_id = this.user.id;
      const medic = await this.userService.createMedic(
        this.user.id,
        this.medic
      );
      this.router.navigateByUrl('/main/tabs/user');
    }
  }

  reset() {
    if (this.medicForm.valid) {
      this.medicForm.reset();
    }
    this.medic = {
      docket: null,
      is_chief: false,
    };
  }
}
