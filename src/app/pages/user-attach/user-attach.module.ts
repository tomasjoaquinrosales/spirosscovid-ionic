import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UserAttachPageRoutingModule } from './user-attach-routing.module';

import { UserAttachPage } from './user-attach.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    UserAttachPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [UserAttachPage],
})
export class UserAttachPageModule {}
