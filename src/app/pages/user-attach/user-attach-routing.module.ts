import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserAttachPage } from './user-attach.page';

const routes: Routes = [
  {
    path: '',
    component: UserAttachPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserAttachPageRoutingModule {}
