import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UserAttachPage } from './user-attach.page';

describe('UserAttachPage', () => {
  let component: UserAttachPage;
  let fixture: ComponentFixture<UserAttachPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserAttachPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UserAttachPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
