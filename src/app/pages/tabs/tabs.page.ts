import { Component } from '@angular/core';
import { PusherProviderService } from '../../services/pusher-provider.service';
import { PusherResponse } from '../../interfaces/pusher.interface';
import { AuthenticationService } from '../../services/authentication.service';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss'],
})
export class TabsPage {
  constructor(
    private pusher: PusherProviderService,
    private authService: AuthenticationService,
    private toastController: ToastController
  ) {
    const channel = this.pusher.init();
    channel.bind('messages', (data: PusherResponse) => {
      if (
        data.active_room.room === this.authService.getSystemName() &&
        data.active_room.active &&
        data.active_room.medics.includes(this.authService.getUsername())
      ) {
        this.pusher.add();
        this.presentToast(data.notification);
      }
    });
  }

  AMIChief() {
    return this.authService.AMIChief();
  }

  getPusherCount() {
    return this.pusher.getCount();
  }

  getSystemName() {
    return this.authService.getSystemName();
  }

  async presentToast(notification) {
    let message = '';
    for (const [key, value] of Object.entries(notification)) {
      message += ` - ${value}`;
    }
    const toast = await this.toastController.create({
      message: `${message}`,
      duration: 2000,
    });
    toast.present();
  }
}
