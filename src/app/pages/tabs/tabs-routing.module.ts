import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
import { OnlyToSystemGuardGuard } from '../../guards/only-to-system-guard.guard';
import { OnlyChiefGuard } from 'src/app/guards/only-chief.guard';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'user',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tab1/tab1.module').then((m) => m.Tab1PageModule),
          },
          {
            path: 'create',
            canActivateChild: [OnlyToSystemGuardGuard],
            loadChildren: () =>
              import('../user-detail/user-detail.module').then(
                (m) => m.UserDetailPageModule
              ),
          },
          {
            path: 'attach/:id',
            loadChildren: () =>
              import('../user-attach/user-attach.module').then(
                (m) => m.UserAttachPageModule
              ),
          },
          {
            path: 'patient/create',
            canActivateChild: [OnlyToSystemGuardGuard],
            loadChildren: () =>
              import('../patient/patient.module').then(
                (m) => m.PatientPageModule
              ),
          },
          {
            path: 'patient/:id/detail',
            loadChildren: () =>
              import('../patient-detail/patient-detail.module').then(
                (m) => m.PatientDetailPageModule
              ),
          },
          {
            path: 'patient/:id/internement/detail',
            loadChildren: () =>
              import(
                '../patient-internement-detail/patient-internement-detail.module'
              ).then((m) => m.PatientInternementDetailPageModule),
          },
          {
            path: 'patient/:id/internement',
            canActivateChild: [OnlyToSystemGuardGuard],
            loadChildren: () =>
              import('../internement-detail/internement-detail.module').then(
                (m) => m.InternementDetailPageModule
              ),
          },
          {
            path: 'patient/:id/evolution',
            loadChildren: () =>
              import('../evolution-detail/evolution-detail.module').then(
                (m) => m.EvolutionDetailPageModule
              ),
          },
          {
            path: 'patient/:id/evolution/detail',
            loadChildren: () =>
              import(
                '../patient-evolution-detail/patient-evolution-detail.module'
              ).then((m) => m.PatientEvolutionDetailPageModule),
          },
          {
            path: 'patient/:id/system',
            loadChildren: () =>
              import('../patient-system/patient-system.module').then(
                (m) => m.PatientSystemPageModule
              ),
          },
          {
            path: 'patient/:id/internement/close',
            loadChildren: () =>
              import(
                '../patient-internemenet-closed/patient-internemenet-closed.module'
              ).then((m) => m.PatientInternemenetClosedPageModule),
          },
        ],
      },
      {
        path: 'dashboard',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tab2/tab2.module').then((m) => m.Tab2PageModule),
          },
        ],
      },
      {
        path: 'notifications',
        loadChildren: () =>
          import('../tab3/tab3.module').then((m) => m.Tab3PageModule),
      },
      {
        path: '',
        redirectTo: '/tabs/user',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: '',
    redirectTo: '/tabs/user',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
