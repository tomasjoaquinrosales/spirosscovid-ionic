import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EvolutionDetailPageRoutingModule } from './evolution-detail-routing.module';

import { EvolutionDetailPage } from './evolution-detail.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    EvolutionDetailPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [EvolutionDetailPage],
})
export class EvolutionDetailPageModule {}
