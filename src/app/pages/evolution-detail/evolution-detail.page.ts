import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from '../../services/users.service';
import { Medic, Patient } from '../../interfaces/users.interfaces';
import {
  Evolution,
  Evolutiontemplate,
  Medicalstudy,
  Treatment,
  Uti,
} from '../../interfaces/evolution.interface';
import { SearchModalPage } from '../search-modal/search-modal.page';
import { ModalController, ToastController } from '@ionic/angular';
import { EvolutionModalPage } from '../evolution-modal/evolution-modal.page';
import { InternementService } from '../../services/internement.service';
import { Internement } from 'src/app/interfaces/internement.interface';
import { EvolutionsService } from 'src/app/services/evolutions.service';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-evolution-detail',
  templateUrl: './evolution-detail.page.html',
  styleUrls: ['./evolution-detail.page.scss'],
})
export class EvolutionDetailPage implements OnInit {
  internement: Internement;
  patient: Patient = {};
  medic: Medic = null;
  evolution: Evolution = {
    medic_id: null,
    internement_id: null,
    evolution_template: null,
    medical_study: null,
    treatment: null,
    uti: null,
  };
  evoultionTemplate: Evolutiontemplate = {
    temperature: null,
    blood_pressures_s: null,
    blood_pressure_d: null,
    cardiac_frequency: null,
    breathing_frequency: null,
    ventilator: null,
    o2: null,
    o2_saturation: null,
    pafi: null,
    cough: false,
    observation: null,
    prono_vigil: false,
    dyspnoea: false,
    drowsiness: false,
    anosmia: false,
    dysguesia: false,
    is_o2_required: false,
    is_pafi_required: false,
  };
  medicalStudy: Medicalstudy;
  treatment: Treatment;
  uti: Uti;
  evolutionForm = new FormGroup({
    temperature: new FormControl(this.evoultionTemplate.temperature, [
      Validators.required,
      Validators.pattern('^[0-9]*$'),
    ]),
    bloodPressureD: new FormControl(this.evoultionTemplate.blood_pressure_d, [
      Validators.required,
      Validators.pattern('^[0-9]*$'),
    ]),
    bloodPressureS: new FormControl(this.evoultionTemplate.blood_pressures_s, [
      Validators.required,
      Validators.pattern('^[0-9]*$'),
    ]),
    cardiacFrequency: new FormControl(
      this.evoultionTemplate.cardiac_frequency,
      [Validators.required, Validators.pattern('^[0-9]*$')]
    ),
    breathingFrequency: new FormControl(
      this.evoultionTemplate.breathing_frequency,
      [Validators.required, Validators.pattern('^[0-9]*$')]
    ),
    ventilator: new FormControl(this.evoultionTemplate.ventilator, [
      Validators.required,
    ]),
    o2: new FormControl(this.evoultionTemplate.o2, [
      Validators.required,
      Validators.minLength(4),
    ]),
    o2Saturation: new FormControl(this.evoultionTemplate.o2_saturation, [
      Validators.required,
      Validators.pattern('^[0-9]*$'),
    ]),
    pafi: new FormControl(this.evoultionTemplate.pafi, [
      Validators.required,
      Validators.pattern('^[0-9]*$'),
    ]),
    cough: new FormControl(this.evoultionTemplate.cough, []),
    observation: new FormControl(this.evoultionTemplate.observation, [
      Validators.required,
    ]),
    pronoVigil: new FormControl(this.evoultionTemplate.prono_vigil, []),
    dyspnoea: new FormControl(this.evoultionTemplate.dyspnoea, []),
    drowsiness: new FormControl(this.evoultionTemplate.drowsiness, []),
    anosmia: new FormControl(this.evoultionTemplate.anosmia, []),
    dysguesia: new FormControl(this.evoultionTemplate.dysguesia, []),
    isO2Required: new FormControl(this.evoultionTemplate.is_o2_required, []),
    isPafiRequired: new FormControl(
      this.evoultionTemplate.is_pafi_required,
      []
    ),
  });

  constructor(
    private routeActive: ActivatedRoute,
    private userService: UsersService,
    private internementService: InternementService,
    private evolutionService: EvolutionsService,
    private modalController: ModalController,
    private route: Router,
    private toastController: ToastController,
    private authService: AuthenticationService
  ) {}

  async ngOnInit() {
    const patientId = Number(this.routeActive.snapshot.paramMap.get('id'));
    this.patient = await this.userService.getPatientById(patientId);
    this.medic = await this.userService.getMedicByUsername(
      this.authService.getUsername()
    );
    this.internement = await this.internementService.getInternementByPatientId(
      patientId
    );
    const evolution = await this.evolutionService.getEvolutionByPatientId(
      patientId
    );
    if (evolution) {
      this.evoultionTemplate = evolution['evolution']['evolution_template'];
      if (this.evoultionTemplate['evolution_date']) {
        this.evoultionTemplate['evolution_date'] = null;
      }
    }
  }

  async presentModalMedicalStudy() {
    const modalMedicalStudy = await this.modalController.create({
      component: EvolutionModalPage,
      componentProps: {
        title: 'Estudio Médico',
        kindOfSearch: 'es',
      },
    });

    modalMedicalStudy.onDidDismiss().then((data) => {
      this.medicalStudy = data['data'];
    });

    return await modalMedicalStudy.present();
  }

  async presentModalTreatment() {
    const modalTreatment = await this.modalController.create({
      component: EvolutionModalPage,
      componentProps: {
        title: 'Tratamiento',
        kindOfSearch: 't',
      },
    });

    modalTreatment.onDidDismiss().then((data) => {
      this.treatment = data['data'];
    });

    return await modalTreatment.present();
  }

  async presentModalUTI() {
    const modalUTI = await this.modalController.create({
      component: EvolutionModalPage,
      componentProps: {
        title: 'UTI',
        kindOfSearch: 'uti',
      },
    });

    modalUTI.onDidDismiss().then((data) => {
      this.uti = data['data'];
    });

    return await modalUTI.present();
  }

  async presentModalMedic() {
    const modalMedic = await this.modalController.create({
      component: SearchModalPage,
      componentProps: {
        title: 'Medicos',
        kindOfSearch: 'medic',
        isMultiCheck: false,
      },
    });

    modalMedic.onDidDismiss().then((data) => {
      this.medic = data['data'] || [];
    });

    return await modalMedic.present();
  }

  removeMedic(medic: Medic) {
    this.medic = null;
    medic = null;
  }

  removeMedicalStudy(medicalStudy) {
    medicalStudy = null;
    this.medicalStudy = null;
  }

  removeTreatment(treatment) {
    treatment = null;
    this.treatment = null;
  }

  removeUTI(uti) {
    uti = null;
    this.uti = null;
  }

  async createEvolution() {
    if (!this.medic) {
      this.presentToast(
        'Para crear una evolución, debe tener un médico a cargo.'
      );
      return;
    }
    if (this.evolutionForm.valid) {
      this.evolution = {
        medic_id: this.medic.id,
        internement_id: this.internement.id,
        evolution_template: this.evoultionTemplate,
        medical_study: this.medicalStudy || null,
        treatment: this.treatment || null,
        uti: this.uti || null,
      };
      const evolution = await this.evolutionService.createEvolution(
        this.evolution
      );
      this.route.navigateByUrl('/main/tabs/user');
    }
  }

  // TOAST
  async presentToast(message) {
    const toast = await this.toastController.create({
      message: `${message}`,
      position: 'middle',
      duration: 2000,
    });
    toast.present();
  }
}
