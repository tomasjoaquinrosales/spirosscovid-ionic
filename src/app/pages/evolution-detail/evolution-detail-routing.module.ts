import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EvolutionDetailPage } from './evolution-detail.page';

const routes: Routes = [
  {
    path: '',
    component: EvolutionDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EvolutionDetailPageRoutingModule {}
