import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EvolutionDetailPage } from './evolution-detail.page';

describe('EvolutionDetailPage', () => {
  let component: EvolutionDetailPage;
  let fixture: ComponentFixture<EvolutionDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvolutionDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EvolutionDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
