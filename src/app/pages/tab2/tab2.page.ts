import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DashboardService } from '../../services/dashboard.service';
import * as Chart from 'chart.js';
import { DashboardResponse, Room } from '../../interfaces/dashboard.interface';
import { Router } from '@angular/router';
import { SystemService } from '../../services/system.service';
import {
  InternementSystemResponse,
  System,
} from 'src/app/interfaces/system-internement.interface';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page implements OnInit {
  @ViewChild('doughnutCanvas') donut: ElementRef;
  chart: Chart;
  rooms: Room[] = [];
  charts: Chart[] = [];
  systems: System[] = [];

  constructor(
    private dashboard: DashboardService,
    private route: Router,
    private systemService: SystemService
  ) {}

  ngOnInit() {
    this.getSystems();
  }

  ionViewWillEnter() {
    this.dashboard
      .getDashboardSummary(null)
      .subscribe((response: DashboardResponse) => {
        this.rooms = response.result.rooms;
      });
  }

  goToPatient(id: number) {
    this.route.navigateByUrl(`/main/tabs/user?patient=${id}`);
  }

  getSystems() {
    this.systemService
      .getSystems()
      .subscribe((response: InternementSystemResponse) => {
        this.systems = response.results;
      });
  }

  findDashboard(event) {
    this.dashboard
      .getDashboardSummary(event.detail.value)
      .subscribe((response: DashboardResponse) => {
        this.rooms = response.result.rooms;
      });
  }
}
