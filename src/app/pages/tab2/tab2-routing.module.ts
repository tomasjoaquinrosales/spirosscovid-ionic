import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OnlyChiefGuard } from 'src/app/guards/only-chief.guard';
import { Tab2Page } from './tab2.page';

const routes: Routes = [
  {
    path: '',
    canActivateChild: [OnlyChiefGuard],
    component: Tab2Page,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Tab2PageRoutingModule {}
