import { Component, OnInit, ViewChild } from '@angular/core';
import { UsersService } from '../../services/users.service';
import { UserProfileListResponse } from '../../interfaces/users.interfaces';
import {
  MedicListResponse,
  PatientListResponse,
  Medic,
  UserProfile,
  Patient,
} from '../../interfaces/users.interfaces';
import { ActivatedRoute, Router } from '@angular/router';
import { IonInfiniteScroll, ToastController } from '@ionic/angular';
import { InternementService } from '../../services/internement.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { PatientInternementDetailPage } from '../patient-internement-detail/patient-internement-detail.page';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  isUser: boolean = false;
  isMedic: boolean = false;
  isPatient: boolean = false;
  users: UserProfile[] = [];
  medics: Medic[] = [];
  patients: Patient[] = [];
  pageUser = 0;
  disabled = false;
  constructor(
    private userService: UsersService,
    private route: Router,
    private internementService: InternementService,
    private toastController: ToastController,
    private authService: AuthenticationService,
    private activateRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.getPatients();
  }

  ionViewDidEnter() {
    this.doRefresh(null);
    this.activateRoute.queryParams.subscribe((params) => {
      if (params['patient']) {
        const patientId = params['patient'];
        this.route.navigate([`/main/tabs/user/patient/${patientId}/detail`], {
          replaceUrl: true,
          queryParams: { patient: PatientInternementDetailPage },
        });
      }
    });
  }

  AMISuperuser() {
    return this.authService.AMISuperuser();
  }

  AMIChief() {
    return this.authService.AMIChief();
  }

  AMIStaff() {
    return this.authService.AMIStaff();
  }

  AMIGuard() {
    return this.authService.AMIGuard();
  }

  private getUsers(event?: any) {
    this.userService
      .getUsers(false)
      .subscribe((response: UserProfileListResponse) => {
        this.users = response.results;
        // this.users.sort((a, b) => (a.id < b.id ? 1 : -1));
        this.isUser = true;
        if (event) {
          event.target.complete();
        }
      });
  }

  private getMedics(event?: any) {
    this.userService
      .getMedics(false)
      .subscribe((response: MedicListResponse) => {
        this.medics = response.results;
        // this.medics.sort((a, b) => (a.id < b.id ? 1 : -1));
        this.isMedic = true;
        if (event) {
          event.target.complete();
        }
      });
  }

  private getPatients(event?: any) {
    this.userService
      .getPatients(false)
      .subscribe((response: PatientListResponse) => {
        this.patients = response.results;
        // this.patients.sort((a, b) => (a.id < b.id ? 1 : -1));
        this.isPatient = true;
        if (event) {
          event.target.complete();
        }
      });
  }

  segmentChanged(ev: any) {
    this.isUser = false;
    this.isMedic = false;
    this.isPatient = false;
    switch (ev.detail.value) {
      case 'user': {
        this.getUsers();
        break;
      }
      case 'medic': {
        this.getMedics();
        break;
      }
      case 'patient': {
        this.getPatients();
        break;
      }
    }
  }

  attachTo(userId) {
    this.route.navigateByUrl(`/main/tabs/user/attach/${userId}`);
  }

  doRefresh(event) {
    if (this.isMedic) {
      this.getMedics(event);
    } else {
      if (this.isPatient) {
        this.getPatients(event);
      } else {
        if (this.isUser) {
          this.getUsers(event);
        }
      }
    }
  }

  async loadData(event) {
    this.userService.getUsers(true).subscribe((response) => {
      if (this.users.length === response.totalResults) {
        event.target.disabled = true;
      } else {
        this.users.push(...response.results);
        event.target.complete();
      }
    });
  }

  private initializeItems(searchTerm: string) {
    return this.userService.getPatients(false, searchTerm).toPromise();
  }

  async filterList(event) {
    let resultSet;
    const searchTerm = event.srcElement.value;
    resultSet = (await this.initializeItems(searchTerm)).results;

    /*if (!searchTerm) {
      this.patients = resultSet;
      return;
    }

    resultSet = resultSet.filter((currentPatient) => {
      if (currentPatient.last_name && searchTerm) {
        return (
          currentPatient.last_name
            .toLowerCase()
            .indexOf(searchTerm.toLowerCase()) > -1
        );
      }
    });*/

    this.patients = resultSet;
  }

  // PATIENT

  async createInternement(patient: Patient) {
    const hasInternement = await this.internementService.hasInternement(
      patient.id
    );
    if (hasInternement) {
      this.presentToast('Este paciente ya tiene una internación activa.');
      return;
    }
    this.route.navigateByUrl(
      `/main/tabs/user/patient/${patient.id}/internement`
    );
  }

  async createEvolution(patient: Patient) {
    const hasInternement = await this.internementService.hasInternement(
      patient.id
    );
    if (!hasInternement) {
      this.presentToast('Este paciente no tiene una internación asignada.');
      return;
    }
    const hasMedic = await this.userService.getMedicByUsername(
      this.authService.getUsername()
    );
    if (!hasMedic) {
      this.presentToast(
        'Usted no es un médico habilitado para relizar esta acción.'
      );
      return;
    }
    this.route.navigateByUrl(`/main/tabs/user/patient/${patient.id}/evolution`);
  }

  patientDetail(patient) {
    this.route.navigateByUrl(`/main/tabs/user/patient/${patient.id}/detail`);
  }

  async finishInternement(patient) {
    const hasInternement = await this.internementService.hasInternement(
      patient.id
    );
    if (!hasInternement) {
      this.presentToast('Este paciente no tiene una internación asignada.');
      return;
    }
    this.route.navigateByUrl(
      `/main/tabs/user/patient/${patient.id}/internement/close`
    );
  }

  // TOAST
  async presentToast(message) {
    const toast = await this.toastController.create({
      message: `${message}`,
      position: 'middle',
      duration: 2000,
    });
    toast.present();
  }
}
