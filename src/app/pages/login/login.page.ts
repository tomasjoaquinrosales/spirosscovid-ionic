import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {
  IonSlides,
  MenuController,
  NavController,
  ViewDidLeave,
  ViewWillEnter,
} from '@ionic/angular';
import { AuthenticationService } from '../../services/authentication.service';
import { LoaderService } from '../../services/loader.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage
  implements OnInit, AfterViewInit, ViewWillEnter, ViewDidLeave {
  avatarSlide = {
    slidesPerView: 3.5,
    initialSlide: 1,
    speed: 400,
    loop: false,
  };
  loginModel = {
    username: '',
    password: '',
  };
  clicked: boolean = false;
  @ViewChild('mainSlide') mainSlide: IonSlides;

  constructor(
    private authService: AuthenticationService,
    private nav: NavController,
    private menu: MenuController,
    private loader: LoaderService
  ) {
    this.menu.enable(false);
  }

  ngOnInit() {}

  ionViewWillEnter() {}

  ionViewDidLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }

  ngAfterViewInit(): void {}

  async login(loginForm: NgForm) {
    if (loginForm.valid) {
      this.clicked = true;
      this.loader.presentLoading();
      const valid = await this.authService.login(
        loginForm.value.username,
        loginForm.value.password
      );

      if (valid) {
        this.clicked = false;
        await this.authService.setRoles();
        await this.loader.dismissLoading();
        this.nav.navigateRoot('/main/tabs/user', { animated: true });
      } else {
        this.clicked = false;
        await this.loader.dismissLoading();
      }
    }
  }

  selectAvatar(avatar) {
    avatar.isSelected = true;
  }
}
