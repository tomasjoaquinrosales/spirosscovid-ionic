import { Component, OnInit } from '@angular/core';
import {
  UserProfile,
  Medic,
  Patient,
  User,
} from '../../interfaces/users.interfaces';
import { UsersService } from '../../services/users.service';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { formatDate } from '@angular/common';
import { ValidationsService } from '../../services/validations.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.page.html',
  styleUrls: ['./user-detail.page.scss'],
})
export class UserDetailPage implements OnInit {
  userProfile: UserProfile = {
    address: null,
    date_of_birth: null,
    dni: null,
    first_name: null,
    last_name: null,
    telephone: null,
  };
  user: User = {
    is_medic: false,
    username: null,
    password: null,
  };

  userForm = new FormGroup({
    dni: new FormControl(this.userProfile.dni, [
      Validators.required,
      Validators.minLength(8),
      Validators.maxLength(8),
    ]),
    firstName: new FormControl(this.userProfile.first_name, [
      Validators.required,
      Validators.minLength(4),
    ]),
    lastName: new FormControl(this.userProfile.last_name, [
      Validators.required,
      Validators.minLength(4),
    ]),
    address: new FormControl(this.userProfile.address, [
      Validators.required,
      Validators.minLength(4),
    ]),
    telephone: new FormControl(this.userProfile.telephone, [
      Validators.required,
    ]),
    dateOfBirth: new FormControl(this.userProfile.date_of_birth, [
      Validators.required,
    ]),
    kindOf: new FormControl('', [Validators.required]),
    username: new FormControl(this.user.username, [
      Validators.required,
      Validators.minLength(4),
    ]),
    password: new FormControl(this.user.password, [
      Validators.required,
      Validators.minLength(8),
    ]),
  });

  stepUser = true;
  kind_of = null;

  constructor(
    private userService: UsersService,
    private route: Router,
    private validationsService: ValidationsService
  ) {}

  ngOnInit() {}

  async validateDNI() {
    let responseValue = await this.validationsService.isFieldUniqueValid(
      'dni',
      this.userProfile.dni
    );
    if (!responseValue) {
      this.userForm.controls['dni'].setErrors({ incorrect: true });
    }
  }

  async validateUsername() {
    let responseValue = await this.validationsService.isFieldUniqueValid(
      'username',
      this.user.username
    );
    if (!responseValue) {
      this.userForm.controls['username'].setErrors({ incorrect: true });
    }
  }

  async createUserProfile() {
    if (this.userForm.valid) {
      const format = 'yyyy-MM-dd';
      const locale = 'en-US';
      const formattedDate = formatDate(
        this.userProfile.date_of_birth,
        format,
        locale
      );
      this.userProfile.date_of_birth = formattedDate;

      if (this.kind_of === 'medic') {
        this.user.is_medic = true;
      }

      const user = await this.userService.createUserProfile(
        this.user,
        this.userProfile
      );
      this.reset();
      this.route.navigateByUrl('/main/tabs/user');
    }
  }

  reset() {
    this.stepUser = true;
    if (this.userForm.valid) {
      this.userForm.reset();
    }
    this.userProfile = {
      address: null,
      date_of_birth: null,
      dni: null,
      first_name: null,
      last_name: null,
      telephone: null,
    };
    this.user = {
      is_medic: false,
      username: null,
      password: null,
    };
  }
}
