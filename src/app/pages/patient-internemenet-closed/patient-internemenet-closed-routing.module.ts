import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PatientInternemenetClosedPage } from './patient-internemenet-closed.page';

const routes: Routes = [
  {
    path: '',
    component: PatientInternemenetClosedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PatientInternemenetClosedPageRoutingModule {}
