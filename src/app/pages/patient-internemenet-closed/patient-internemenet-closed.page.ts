import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { InternementService } from 'src/app/services/internement.service';
import { Internement } from '../../interfaces/internement.interface';

@Component({
  selector: 'app-patient-internemenet-closed',
  templateUrl: './patient-internemenet-closed.page.html',
  styleUrls: ['./patient-internemenet-closed.page.scss'],
})
export class PatientInternemenetClosedPage implements OnInit {
  internement: Internement = {
    check_out: null,
    date_of_death: null,
    death_observation: null,
  };
  internementCloseForm = new FormGroup({
    checkOut: new FormControl(this.internement.check_out, []),
    dateOfDeath: new FormControl(this.internement.date_of_death, []),
    deathObservation: new FormControl(this.internement.death_observation, [
      Validators.required,
    ]),
  });
  patientId: Number;

  constructor(
    private internementService: InternementService,
    private route: Router,
    private routeActive: ActivatedRoute
  ) {
    this.patientId = Number(this.routeActive.snapshot.paramMap.get('id'));
  }

  ngOnInit() {}

  private transformDate(date) {
    const format = 'yyyy-MM-dd';
    const locale = 'en-US';
    const formattedDate = formatDate(date, format, locale);
    return formattedDate;
  }

  async closeInternement() {
    if (this.internementCloseForm.valid) {
      if (this.internement.check_out) {
        this.internement.check_out = this.transformDate(
          this.internement.check_out
        );
      }
      if (this.internement.date_of_death) {
        this.internement.date_of_death = this.transformDate(
          this.internement.date_of_death
        );
      }
      const internement = await this.internementService.closeInternement(
        this.internement,
        this.patientId
      );
      this.route.navigateByUrl(`/main/tabs/user`);
    }
  }
}
