import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PatientInternemenetClosedPageRoutingModule } from './patient-internemenet-closed-routing.module';

import { PatientInternemenetClosedPage } from './patient-internemenet-closed.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PatientInternemenetClosedPageRoutingModule,
    ComponentsModule,
    ReactiveFormsModule,
  ],
  declarations: [PatientInternemenetClosedPage],
})
export class PatientInternemenetClosedPageModule {}
