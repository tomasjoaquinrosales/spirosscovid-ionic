import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PatientInternemenetClosedPage } from './patient-internemenet-closed.page';

describe('PatientInternemenetClosedPage', () => {
  let component: PatientInternemenetClosedPage;
  let fixture: ComponentFixture<PatientInternemenetClosedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientInternemenetClosedPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PatientInternemenetClosedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
