import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PatientInternementDetailPageRoutingModule } from './patient-internement-detail-routing.module';

import { PatientInternementDetailPage } from './patient-internement-detail.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PatientInternementDetailPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [PatientInternementDetailPage],
})
export class PatientInternementDetailPageModule {}
