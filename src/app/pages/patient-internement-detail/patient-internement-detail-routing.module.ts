import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PatientInternementDetailPage } from './patient-internement-detail.page';

const routes: Routes = [
  {
    path: '',
    component: PatientInternementDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PatientInternementDetailPageRoutingModule {}
