import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IonInfiniteScroll } from '@ionic/angular';
import { Internement } from 'src/app/interfaces/internement.interface';
import { LoaderService } from 'src/app/services/loader.service';
import { InternementService } from '../../services/internement.service';

@Component({
  selector: 'app-patient-internement-detail',
  templateUrl: './patient-internement-detail.page.html',
  styleUrls: ['./patient-internement-detail.page.scss'],
})
export class PatientInternementDetailPage implements OnInit {
  patientId: number;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  internements: Internement[] = [];
  loading = true;

  constructor(
    private internementService: InternementService,
    private routeActive: ActivatedRoute,
    private loader: LoaderService
  ) {
    this.patientId = Number(this.routeActive.snapshot.paramMap.get('id'));
  }

  ngOnInit() {
    this.getInternements();
  }

  private getInternements(event?: any) {
    this.loader.presentLoading();
    this.loading = true;
    this.internementService
      .getInternements(false, this.patientId)
      .subscribe((response) => {
        this.internements = response.results;
        if (event) {
          event.target.complete();
        }
        this.loading = false;
        this.loader.dismissLoading();
      });
  }

  doRefresh(event) {
    this.getInternements(event);
  }

  create() {}

  async loadData(event) {
    this.internementService
      .getInternements(true, this.patientId)
      .subscribe((response) => {
        if (this.internements.length === response.totalResults) {
          event.target.disabled = true;
        } else {
          this.internements.push(...response.results);
        }
        event.target.complete();
      });
  }
}
