import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PatientSystemPage } from './patient-system.page';

const routes: Routes = [
  {
    path: '',
    component: PatientSystemPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PatientSystemPageRoutingModule {}
