import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PatientSystemPage } from './patient-system.page';

describe('PatientSystemPage', () => {
  let component: PatientSystemPage;
  let fixture: ComponentFixture<PatientSystemPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientSystemPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PatientSystemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
