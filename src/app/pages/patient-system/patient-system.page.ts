import { Component, OnInit } from '@angular/core';
import {
  InternementSystem,
  InternementSystemResponse,
} from 'src/app/interfaces/system-internement.interface';
import { SystemService } from '../../services/system.service';
import { System } from '../../interfaces/system-internement.interface';
import { AuthenticationService } from '../../services/authentication.service';
import {
  SystemBed,
  SystemResponse,
  SystemRoom,
} from 'src/app/interfaces/system.interface';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { InternementService } from '../../services/internement.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-patient-system',
  templateUrl: './patient-system.page.html',
  styleUrls: ['./patient-system.page.scss'],
})
export class PatientSystemPage implements OnInit {
  systems: System[] = [];
  userSystemName: string;
  systemNew: InternementSystem = {
    internement_id: null,
    system_id: null,
    bed_id: null,
    bed_label: null,
    medic_id: null,
  };
  roomOptions: any = {
    header: 'Salas Disponibles',
    message: 'Si no hay camas disponibles, puede crear una en fuera de sala.',
    translucent: true,
  };
  bedOptions: any = {
    header: 'Camas Disponibles',
    message: 'Si no hay camas disponibles, puede crear una en fuera de sala.',
    translucent: true,
  };
  selectBedDisabled = true;
  rooms: SystemRoom[] = [];
  beds: SystemBed[] = [];
  changeForm = new FormGroup({
    system_id: new FormControl('', [Validators.required]),
    room: new FormControl('', [Validators.required]),
    bed_id: new FormControl('', []),
    bed_label: new FormControl('', []),
  });
  hasBeds = false;
  selectNewBedDisabled = true;
  newBedLabel = null;

  constructor(
    private systemService: SystemService,
    private authServices: AuthenticationService,
    private internementService: InternementService,
    private route: ActivatedRoute,
    private router: Router,
    private toastController: ToastController
  ) {}

  ngOnInit() {
    this.userSystemName = this.authServices.getSystemName();
    const patientId = this.route.snapshot.params['id'];
    this.systemService
      .getSystemsByPatientId(patientId)
      .subscribe((response: InternementSystemResponse) => {
        this.systems = response.results;
      });
    this.internementService
      .getInternementByPatientId(patientId)
      .then((response) => {
        this.systemNew.internement_id = response.id;
      });
  }

  loadBeds(event) {
    if (!this.hasBeds) {
      this.presentToast('No hay camas en el sistema destino.');
      this.rooms = [];
      return;
    }
    this.systemNew.room_id = event.detail.value.id;
    this.selectBedDisabled = true;
    this.systemService.getBeds(event.detail.value.id).subscribe((response) => {
      this.beds = response.response;
      if (event.detail.value.is_room_infinite && this.beds.length === 0) {
        this.selectNewBedDisabled = false;
      } else {
        this.selectBedDisabled = false;
      }
    });
  }

  async verifyBeds(event) {
    this.hasBeds = await this.systemService.validateSystemBed(
      event.detail.value
    );
    if (!this.hasBeds) {
      this.presentToast('No hay camas en el sistema destino.');
      this.rooms = [];
      return;
    }
    this.systemService
      .getRooms(event.detail.value)
      .subscribe((response: SystemResponse) => {
        this.rooms = response.response;
        console.log(this.rooms);
      });
  }

  async changePatientSystem() {
    if (this.changeForm.valid) {
      if (!this.hasBeds) {
        this.presentToast('No hay camas en el sistema destino.');
        return;
      }
      if (this.beds.length == 0 && this.newBedLabel == null) {
        this.presentToast('Debe escribir su dirección.');
        return;
      }
      this.systemNew.bed_label = this.newBedLabel;
      const internementSystem = await this.internementService.changeInternementSystem(
        this.systemNew
      );
      this.router.navigateByUrl(`/main/tabs/user`);

      this.systemNew = {
        internement_id: null,
        system_id: null,
        bed_id: null,
        bed_label: null,
        medic_id: null,
      };
    }
  }

  addBedToList() {
    const bed = { label: this.newBedLabel, id: null };
    this.beds.push(bed);
    this.selectBedDisabled = false;
    this.selectNewBedDisabled = true;
  }

  async presentToast(message) {
    const toast = await this.toastController.create({
      message: `${message}`,
      position: 'middle',
      duration: 2000,
    });
    toast.present();
  }
}
