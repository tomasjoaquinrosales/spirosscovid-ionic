import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PatientSystemPageRoutingModule } from './patient-system-routing.module';

import { PatientSystemPage } from './patient-system.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PatientSystemPageRoutingModule,
    ComponentsModule,
    ReactiveFormsModule,
  ],
  declarations: [PatientSystemPage],
})
export class PatientSystemPageModule {}
