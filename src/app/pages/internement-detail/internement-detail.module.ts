import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InternementDetailPageRoutingModule } from './internement-detail-routing.module';

import { InternementDetailPage } from './internement-detail.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    InternementDetailPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [InternementDetailPage],
})
export class InternementDetailPageModule {}
