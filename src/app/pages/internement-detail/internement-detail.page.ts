import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController, AlertController } from '@ionic/angular';
import { UsersService } from '../../services/users.service';
import { SearchModalPage } from '../search-modal/search-modal.page';
import { User, Medic, Patient } from '../../interfaces/users.interfaces';
import { formatDate } from '@angular/common';
import { InternementService } from '../../services/internement.service';
import { Internement } from 'src/app/interfaces/internement.interface';
import { Router, ActivatedRoute } from '@angular/router';
import { SystemService } from '../../services/system.service';
import {
  SystemResponse,
  SystemRoom,
  SystemBed,
} from '../../interfaces/system.interface';

@Component({
  selector: 'app-internement-detail',
  templateUrl: './internement-detail.page.html',
  styleUrls: ['./internement-detail.page.scss'],
})
export class InternementDetailPage implements OnInit {
  today = new Date();
  roomOptions: any = {
    header: 'Salas Disponibles',
    message: 'Si no hay camas disponibles, puede crear una en fuera de sala.',
    translucent: true,
  };
  bedOptions: any = {
    header: 'Camas Disponibles',
    message: 'Si no hay camas disponibles, puede crear una en fuera de sala.',
    translucent: true,
  };
  selectBedDisabled = true;
  rooms: SystemRoom[] = [];
  beds: SystemBed[] = [];
  patient: Patient = {
    created: '',
    id: 0,
    is_active: false,
    address: '',
    medical_coverage: '',
    modified: '',
    other_information: '',
    date_of_birth: '',
    dni: '',
    first_name: '',
    last_name: '',
    telephone: '',
  };
  medics: Medic[];
  internement: Internement = {
    current_disease: null,
    date_first_simptoms: null,
    date_diagnosis: null,
    check_in: null,
    check_out: null,
    date_of_death: null,
    patient_id: null,
    medics_ids: null,
    bed_id: null,
    bed_label: null,
  };
  selectNewBedDisabled = true;
  newBedLabel = null;

  internementForm = new FormGroup({
    currentDisease: new FormControl(this.internement.current_disease, [
      Validators.required,
      Validators.minLength(4),
    ]),
    dateFirstSymptoms: new FormControl(this.internement.date_first_simptoms, [
      Validators.required,
    ]),
    dataDiagnosis: new FormControl(
      { value: this.internement.date_diagnosis, disabled: true },
      [Validators.required]
    ),
    checkOut: new FormControl(
      { value: this.internement.check_out, disabled: true },
      []
    ),
    dateOfDeath: new FormControl(
      { value: this.internement.date_of_death, disabled: true },
      []
    ),
    room: new FormControl(this.internement.room_id, [Validators.required]),
    bed: new FormControl(
      { value: this.internement.bed_id, disabled: this.selectBedDisabled },
      [Validators.required]
    ),
    bed_label: new FormControl({ value: this.internement.bed_label }, []),
  });

  constructor(
    private routeActive: ActivatedRoute,
    private modalController: ModalController,
    private alertController: AlertController,
    private internementService: InternementService,
    private route: Router,
    private userService: UsersService,
    private systemService: SystemService
  ) {
    this.systemService.getRooms().subscribe((response: SystemResponse) => {
      this.rooms = response.response;
    });
  }

  async ngOnInit() {
    const patientId = Number(this.routeActive.snapshot.paramMap.get('id'));
    this.patient = await this.userService.getPatientById(patientId);
  }

  async presentAlert(errorMessage: string) {
    const alert = await this.alertController.create({
      header: 'Error',
      message: `${errorMessage}`,
      buttons: ['OK'],
    });

    await alert.present();
  }

  async presentModalMedic() {
    const modalMedic = await this.modalController.create({
      component: SearchModalPage,
      componentProps: {
        title: 'Medicos',
        kindOfSearch: 'medic',
        isMultiCheck: true,
      },
    });

    modalMedic.onDidDismiss().then((data) => {
      this.medics = data['data'];
    });

    return await modalMedic.present();
  }

  private transformDate(date) {
    const format = 'yyyy-MM-dd';
    const locale = 'en-US';
    const formattedDate = formatDate(date, format, locale);
    return formattedDate;
  }

  async createInternement() {
    if (!this.patient) {
      this.presentAlert('El paciente es obligatorio para este formulario.');
      return;
    }
    if (!this.medics) {
      this.presentAlert('El menos un médico debe ser seleccionado.');
      return;
    }
    if (this.internementForm.valid) {
      this.internement.check_in = this.transformDate(this.today);
      this.internement.date_diagnosis = this.transformDate(
        this.internement.date_diagnosis
      );
      this.internement.date_first_simptoms = this.transformDate(
        this.internement.date_first_simptoms
      );
      if (this.internement.date_of_death) {
        this.internement.date_of_death = this.transformDate(
          this.internement.date_of_death
        );
      }
      if (this.internement.check_out) {
        this.internement.check_out = this.transformDate(
          this.internement.check_out
        );
      }
      this.internement.patient_id = this.patient.id;
      this.internement.medics_ids = [];
      this.medics.forEach((medic) => {
        this.internement.medics_ids.push(medic.id);
      });
      this.internement.bed_label = this.newBedLabel;
      const internement = await this.internementService.createInternement(
        this.internement
      );
      this.route.navigate([
        `/main/tabs/user/patient/${this.patient.id}/detail`,
        { replaceUrl: true },
      ]);
    }
  }

  reset() {
    this.internementForm.reset();
    this.patient = {
      created: '',
      id: 0,
      is_active: false,
      address: '',
      medical_coverage: '',
      modified: '',
      other_information: '',
      date_of_birth: '',
      dni: '',
      first_name: '',
      last_name: '',
      telephone: '',
    };
    this.medics = [];
    this.internement = {
      current_disease: null,
      date_first_simptoms: null,
      date_diagnosis: null,
      check_in: null,
      check_out: null,
      date_of_death: null,
      patient_id: null,
      medics_ids: null,
    };
  }

  removeMedic(medic: Medic) {
    if (this.medics.includes(medic)) {
      const pos = this.medics.findIndex((elem) => elem === medic);
      this.medics.splice(pos, 1);
    }
  }

  loadBeds(event) {
    this.internement.room_id = event.detail.value.id;
    this.selectBedDisabled = true;
    this.systemService.getBeds(event.detail.value.id).subscribe((response) => {
      this.beds = response.response;
      this.selectBedDisabled = false;
      if (event.detail.value.is_room_infinite && this.beds.length === 0) {
        this.selectNewBedDisabled = false;
      }
    });
  }

  addBedToList() {
    const bed = { label: this.newBedLabel, id: null };
    this.beds.push(bed);
    this.selectNewBedDisabled = true;
  }
}
