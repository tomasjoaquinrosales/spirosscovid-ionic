import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InternementDetailPage } from './internement-detail.page';

describe('InternementDetailPage', () => {
  let component: InternementDetailPage;
  let fixture: ComponentFixture<InternementDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternementDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InternementDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
