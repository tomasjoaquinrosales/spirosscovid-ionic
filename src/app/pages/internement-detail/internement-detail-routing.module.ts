import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InternementDetailPage } from './internement-detail.page';

const routes: Routes = [
  {
    path: '',
    component: InternementDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InternementDetailPageRoutingModule {}
