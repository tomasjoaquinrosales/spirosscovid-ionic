import { Component, OnInit } from '@angular/core';
import { Patient } from '../../interfaces/users.interfaces';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from '../../services/users.service';
import { InternementService } from '../../services/internement.service';
import { ModalController, ToastController } from '@ionic/angular';
import { AuthenticationService } from '../../services/authentication.service';
import { SearchModalPage } from '../search-modal/search-modal.page';

@Component({
  selector: 'app-patient-detail',
  templateUrl: './patient-detail.page.html',
  styleUrls: ['./patient-detail.page.scss'],
})
export class PatientDetailPage implements OnInit {
  patient: Patient = {
    created: '',
    id: 0,
    is_active: false,
    address: '',
    medical_coverage: '',
    modified: '',
    other_information: '',
    date_of_birth: '',
    dni: '',
    first_name: '',
    last_name: '',
    telephone: '',
    room: null,
  };
  backUrl: string = '/main/tabs/user';
  isInternementValid: boolean = true;
  patientId: number;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UsersService,
    private internementService: InternementService,
    private toastController: ToastController,
    private authService: AuthenticationService,
    private modalController: ModalController
  ) {}

  AMIGuard() {
    return this.authService.AMIGuard();
  }

  async ngOnInit() {
    this.patientId = this.route.snapshot.params['id'];
    this.patient = await this.userService.getPatientById(this.patientId);
    this.verifyInternement();
  }

  ionViewWillEnter() {
    this.verifyInternement();
  }

  async verifyInternement() {
    const internement = await this.internementService.hasInternement(
      this.patientId
    );
    if (internement) {
      this.isInternementValid = false;
    }
  }

  async createInternement() {
    if (this.AMIGuard()) {
      if (!this.isInternementValid) {
        this.presentToast('Este paciente ya tiene una internación activa.');
        return;
      }
      this.router.navigateByUrl(
        `/main/tabs/user/patient/${this.patient.id}/internement`
      );
    } else {
      this.router.navigateByUrl(`/main/tabs/user`);
    }
  }

  async createEvolution() {
    if (this.isInternementValid) {
      this.presentToast('Este paciente no tiene una internación asignada.');
      return;
    }
    this.router.navigateByUrl(
      `/main/tabs/user/patient/${this.patient.id}/evolution`
    );
  }

  async presentModalMedic() {
    if (this.isInternementValid) {
      this.presentToast('Este paciente no tiene una internación asignada.');
      return;
    }
    const modalMedic = await this.modalController.create({
      component: SearchModalPage,
      componentProps: {
        title: 'Medicos',
        kindOfSearch: 'medic',
        isMultiCheck: true,
      },
    });

    modalMedic.onDidDismiss().then((data) => {
      const medics = data['data'];
      if (medics) {
        this.internementService
          .addMedicsToInternement(this.patientId, medics)
          .then((response) => {
            this.presentToast('Médicos asignados.');
          })
          .catch((error) => {
            this.presentToast('Error. Contactese con IT.');
          });
      }
    });

    return await modalMedic.present();
  }

  // TOAST
  async presentToast(message) {
    const toast = await this.toastController.create({
      message: `${message}`,
      position: 'middle',
      duration: 2000,
    });
    toast.present();
  }
}
