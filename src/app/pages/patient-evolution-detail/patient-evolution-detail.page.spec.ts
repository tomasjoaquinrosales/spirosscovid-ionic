import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PatientEvolutionDetailPage } from './patient-evolution-detail.page';

describe('PatientEvolutionDetailPage', () => {
  let component: PatientEvolutionDetailPage;
  let fixture: ComponentFixture<PatientEvolutionDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientEvolutionDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PatientEvolutionDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
