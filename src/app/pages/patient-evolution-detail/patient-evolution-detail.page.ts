import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-patient-evolution-detail',
  templateUrl: './patient-evolution-detail.page.html',
  styleUrls: ['./patient-evolution-detail.page.scss'],
})
export class PatientEvolutionDetailPage implements OnInit {
  patientId: number;

  constructor(private routeActive: ActivatedRoute) {
    this.patientId = Number(this.routeActive.snapshot.paramMap.get('id'));
  }

  ngOnInit() {}
}
