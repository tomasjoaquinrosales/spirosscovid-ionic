import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PatientEvolutionDetailPageRoutingModule } from './patient-evolution-detail-routing.module';

import { PatientEvolutionDetailPage } from './patient-evolution-detail.page';
import { TimelineComponent } from './timeline/timeline.component';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PatientEvolutionDetailPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [PatientEvolutionDetailPage, TimelineComponent],
})
export class PatientEvolutionDetailPageModule {}
