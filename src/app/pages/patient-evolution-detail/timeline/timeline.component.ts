import { Component, Input, OnInit } from '@angular/core';
import { Evolution } from 'src/app/interfaces/evolution.interface';
import { EvolutionsService } from '../../../services/evolutions.service';
import { ModalController } from '@ionic/angular';
import { InformationModalComponent } from '../../../components/information-modal/information-modal.component';
import {
  Medicalstudy,
  Treatment,
  Uti,
} from '../../../interfaces/evolution.interface';
import { Router } from '@angular/router';
import { Medic } from 'src/app/interfaces/users.interfaces';
import { LoaderService } from 'src/app/services/loader.service';
import {
  EvolutionHistory,
  EvolutionHistoryResponse,
} from '../../../interfaces/evolution-hisotry.interface';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss'],
})
export class TimelineComponent implements OnInit {
  @Input() patientId: number;
  evolutions: EvolutionHistory[] = [];
  loading = true;

  constructor(
    private evolutionService: EvolutionsService,
    private modalController: ModalController,
    public router: Router,
    private loader: LoaderService
  ) {}

  ngOnInit() {
    this.loader.presentLoading();
    this.loading = true;
    this.evolutionService
      .getEvolutionHistory(this.patientId)
      .subscribe((response: EvolutionHistoryResponse) => {
        this.evolutions = response.results;
        this.loading = false;
        this.loader.dismissLoading();
      });
  }

  async presentModal(
    title: string,
    ms?: Medicalstudy,
    t?: Treatment,
    uti?: Uti,
    m?: Medic
  ) {
    let information = {};
    if (ms) {
      information = {
        id: ms.id,
        name: ms.name,
        normal: ms.is_normal,
        patological: ms.is_pathological,
      };
    } else if (t) {
      information = {
        id: t.id,
        name: t.name,
        description: t.description,
      };
    } else if (uti) {
      information = {
        id: uti.id,
        arm: uti.arm,
        arm_description: uti.arm_description,
        tracheostomy: uti.tracheostomy,
        vasopressors: uti.vasopressors,
        vasopressors_description: uti.vasopressors_description,
      };
    } else if (m) {
      information = {
        id: m.id,
        docket: m.docket,
      };
    }
    const modal = await this.modalController.create({
      component: InformationModalComponent,
      cssClass: 'modalCss',
      componentProps: {
        title: title,
        info: information,
      },
    });
    return await modal.present();
  }
}
