import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PatientEvolutionDetailPage } from './patient-evolution-detail.page';

const routes: Routes = [
  {
    path: '',
    component: PatientEvolutionDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PatientEvolutionDetailPageRoutingModule {}
