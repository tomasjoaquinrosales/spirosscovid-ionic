import { Directive } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
  selector: '[appUniqueKeyModel]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: UniqueKeyModelDirective,
      multi: true,
    },
  ],
})
export class UniqueKeyModelDirective implements Validator {
  validate(control: AbstractControl): { [key: string]: any } | null {
    if (control.value && control.value.length != 10) {
      return { phoneNumberInvalid: true }; // return object if the validation is not passed.
    }
    return null; // return null if validation is passed.
  }
}
