import { Injectable } from '@angular/core';
import {
  CanActivateChild,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root',
})
export class OnlyToSystemGuardGuard implements CanActivateChild {
  constructor(
    private auth: AuthenticationService,
    private nav: NavController
  ) {}

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    if (!this.auth.AMIGuard()) {
      this.nav.navigateRoot('/main/tabs/user', { animated: true });
    }
    return this.auth.AMIGuard();
  }
}
