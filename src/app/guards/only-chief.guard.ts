import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({
  providedIn: 'root',
})
export class OnlyChiefGuard implements CanActivate {
  constructor(
    private auth: AuthenticationService,
    private nav: NavController
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    console.log('HOLIS');
    console.log(this.auth.AMIChief());
    if (!this.auth.AMIChief()) {
      this.nav.navigateRoot('/main/tabs/dashboard', { animated: true });
      return true;
    }
    this.nav.navigateRoot('/main/tabs/user', { animated: true });
    return false;
  }
}
