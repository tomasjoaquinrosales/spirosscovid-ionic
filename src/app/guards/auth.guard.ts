import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  CanLoad,
  Route,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';
import { NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    private auth: AuthenticationService,
    private nav: NavController
  ) {}

  canActivate(): Promise<boolean> {
    return this.auth.isAuthenticated().then((isValid) => {
      if (!isValid) {
        this.nav.navigateRoot('/login', { animated: true });
      }
      return Promise.resolve(isValid);
    });
  }
}
