import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {
  constructor(private router: Router) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError((err) => {
        if (err instanceof HttpErrorResponse) {
          if (err.error instanceof ErrorEvent) {
            // console.error('Error Event');
            // client-side code may fail to generate the request and throw the error
          } else {
            switch (err.status) {
              case 401:
                this.router.navigateByUrl('/login');
                break;
              case 403:
                // this.router.navigateByUrl("/unauthorized");
                break;
            }
          }
        } else {
          console.error('some thing else happened');
        }
        return throwError(err);
      })
    );
  }
}
