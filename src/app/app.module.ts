import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// HTTP
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// PIPES
import { PipesModule } from './pipes/pipes.module';

// Interceptors
import { httpInterceptorProviders } from './interceptors/interceptors';
import { AppErrorHandler } from './errors/app-error-handler';

// Storage
import { IonicStorageModule } from '@ionic/storage';

// File
import { File } from '@ionic-native/file/ngx';

// TO CONVERT IMAGE
import { Base64 } from '@ionic-native/base64/ngx';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { UniqueKeyModelDirective } from './directives/unique-key-model.directive';

@NgModule({
  declarations: [AppComponent, UniqueKeyModelDirective],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    PipesModule,
    IonicStorageModule.forRoot(),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
    }),
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    httpInterceptorProviders,
    { provide: ErrorHandler, useClass: AppErrorHandler },
    File,
    Base64,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
