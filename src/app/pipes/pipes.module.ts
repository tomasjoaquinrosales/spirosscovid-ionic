import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImagePipe } from './image.pipe';
import { DomSanitizerPipe } from './dom-sanitizer.pipe';
import { ValueArrayPipe } from './value-array.pipe';

@NgModule({
  declarations: [ImagePipe, DomSanitizerPipe, ValueArrayPipe],
  exports: [ImagePipe, DomSanitizerPipe, ValueArrayPipe],
  imports: [CommonModule],
})
export class PipesModule {}
