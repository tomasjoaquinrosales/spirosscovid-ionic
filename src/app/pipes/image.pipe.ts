import { Pipe, PipeTransform } from '@angular/core';
import { environment } from '../../environments/environment';

const URL = environment.url;

@Pipe({
  name: 'image',
})
export class ImagePipe implements PipeTransform {
  transform(path: string): string {
    if (path !== undefined || path !== '') {
      return `${URL}${path}`;
    }

    return '/assets/shapes.svg';
  }
}
